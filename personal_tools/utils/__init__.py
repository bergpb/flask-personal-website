import sqlite3
from random import randint


def return_phrase():
    with sqlite3.connect("dql.db") as con:
        cur = con.cursor()
        cur.execute(
            """SELECT w.name, w.gender_id, wotd.date FROM washer_of_the_day wotd INNER JOIN washers w on w.sequence = wotd.washer_id WHERE wotd.date = date("now", "localtime")"""
        )

        data = cur.fetchone()

        current_washer = data[0]
        gender = data[1]
        date = data[2]

        names = ["Brenda", "Cilene", "Raimundo", "Lindembergue"]

        phrases = [
            "hoje é seu dia de lavar a louça!",
            "hoje não é seu dia, hoje é o dia",
            "prepare-se, hoje é seu dia de lavar a louça!",
        ]

        rand_phrase = randint(0, len(phrases) - 1)
        phrase = phrases[rand_phrase]
        pronoun_gender = "do" if gender == 0 else "da"

        list_without_current_washer = [x for x in names if x != current_washer]
        rand_name = list_without_current_washer[
            randint(0, len(list_without_current_washer) - 1)
        ]

        if rand_phrase == 1:
            phrase = f"{rand_name}, {phrases[rand_phrase]} {pronoun_gender} {current_washer}"
        else:
            phrase = f"{current_washer}, {phrase}"

        data = {"phrase": phrase, "date": date}

        return data
