"""Some functions to format strings"""


def return_name_of_coin(coin_name: str):
    if coin_name == "dolar":
        return "Dólar"
    elif coin_name == "dolar_turismo":
        return "Dólar Turismo"
    return coin_name.replace("_", " ").title()
