function toggle(){
    let darkLink = document.getElementById('dark-sheet');
    if(darkLink){
        darkLink.remove();
        window.localStorage.setItem('theme', 'light');
    } else{
        darkLink = document.createElement('link');
        darkLink.rel = 'stylesheet';
        darkLink.id = 'dark-sheet';
        darkLink.href = '/static/css/bulma-prefers-dark.css'
        document.head.appendChild(darkLink);
        window.localStorage.setItem('theme', 'dark');
    }
}
