import os
from flask import Flask
from dotenv import load_dotenv

from .extensions.mongo import mongodb
from .extensions.debug_toolbar import toolbar

from .blueprints.errors import errors
from .blueprints.dql_api import dql_api
from .blueprints.fv_api import fv_api
from .blueprints.site import site
from .blueprints.coins import coins
from .blueprints.ci import update


def create_app():
    app = Flask(__name__)

    basedir = os.path.abspath(os.path.dirname(__file__))
    load_dotenv(os.path.join(basedir, ".env"))

    app.config.from_object(
        "config." + os.environ.get("FLASK_ENV", "development")
    )

    mongodb.init_app(app)
    toolbar.init_app(app)

    app.register_blueprint(site)
    app.register_blueprint(coins)
    app.register_blueprint(dql_api)
    app.register_blueprint(fv_api)
    app.register_blueprint(update)
    app.register_blueprint(errors)

    return app
