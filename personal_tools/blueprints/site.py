from os import getenv
from flask import Blueprint, render_template

site = Blueprint("site", __name__, url_prefix="/")

version = getenv("VERSION", "")
environment = getenv("FLASK_ENV", "")


@site.get("/")
def index():
    data = {"version": version, "environment": environment}
    return render_template("main/index.html", **data)
