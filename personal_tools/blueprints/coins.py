from os import getenv
from personal_tools import mongodb
from personal_tools.utils import filters
from personal_tools.utils.formatting import return_name_of_coin
from flask import Blueprint, render_template


coins = Blueprint("coins", __name__, url_prefix="/coins")
coins.add_app_template_filter(filters.format_coin)
coins.add_app_template_filter(filters.add_underscore)
coins.add_app_template_filter(filters.remove_special_characters)

version = getenv("VERSION", "")
environment = getenv("FLASK_ENV", "")


def get_coin(coin, limit=200):
    dates = [
        item["datetime"].strftime("%m/%d/%Y %H:%M")
        for item in mongodb.db.coins.find({"name": coin}, {"datetime": 1})
        .sort("_id", -1)
        .limit(limit)
    ]

    values = [
        item["value"]
        for item in mongodb.db.coins.find({"name": coin}, {"value": 1})
        .sort("_id", -1)
        .limit(limit)
    ]

    data = {
        "name": coin,
        "dates": dates,
        "values": values,
    }

    return data


@coins.get("/")
def index():
    coins = mongodb.db.coins.find().sort("_id", -1).limit(12)
    coins_bc_purchase = (
        mongodb.db.coins.find({"name": "Dólar BC - Compra"})
        .sort("_id", -1)
        .limit(1)[0]
    )
    coins_bc_sale = (
        mongodb.db.coins.find({"name": "Dólar BC - Venda"})
        .sort("_id", -1)
        .limit(1)[0]
    )
    footer = f"Updated at: {coins[0]['datetime'].strftime('%d/%m/%Y - %H:%M')}"
    return render_template(
        "coins/index.html",
        purchase=coins_bc_purchase,
        sale=coins_bc_sale,
        data=coins,
        footer=footer,
    )


@coins.get("/<coin_name>")
def return_coin(coin_name):
    coin_name = return_name_of_coin(coin_name)
    data = {
        "coins": get_coin(coin_name),
        "version": version,
        "environment": environment,
    }
    return render_template("coins/chart.html", **data)
