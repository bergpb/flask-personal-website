from flask import Blueprint, request
import git
from pathlib import Path


update = Blueprint("update", __name__)


@update.post("/update")
def webhook():
    if request.method == "POST":
        repo = git.Repo("/home/bergpb/bergpb.pythonanywhere.com")
        origin = repo.remotes.origin
        origin.pull()
        Path("/var/www/bergpb_pythonanywhere_com_wsgi.py").touch()
        return "Yeah! Updated successfully!", 200
    else:
        return "Ops! Wrong event type", 400
