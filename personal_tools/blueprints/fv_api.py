import requests
from random import randrange
from flask import Blueprint, jsonify, g


fv_api = Blueprint("fv_api", __name__, url_prefix="/api")

__URL__ = "https://gist.githubusercontent.com/bergpb/76f23a2aab91ff09660ae1319e0b6f56/raw/frases_christian_profanus.txt"


@fv_api.before_request
def before_request_func():
    phrases = requests.get(__URL__).text
    list_phrases = phrases.split("\n")
    g.phrase = list_phrases[randrange(len(list_phrases))]


@fv_api.get("/return_quote")
def index():
    data = {
        "success": True,
        "quote": g.phrase,
        "author": "Profanus, Christian Cardoso",
    }
    res = jsonify(data)
    return res
