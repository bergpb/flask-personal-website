import os
import time
import cfscrape
import requests
from dotenv import load_dotenv
from bs4 import BeautifulSoup
from pymongo import MongoClient
from schedule import every, repeat, run_pending
from datetime import datetime as dt

basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(basedir, "personal_tools", ".env"))

db = MongoClient(os.environ.get("MONGO_URI"))
scraper = cfscrape.create_scraper()

coins_collection = db["ravena"]["coins"]

DELAY = int(os.environ.get("DELAY"))
CHECK_DOLAR_BC = os.environ.get("CHECK_DOLAR_BC")
CHECK_DOLAR_BC_URL = os.environ.get("CHECK_DOLAR_BC_URL")

coins_data = [
    {"name": "Dólar", "url": "https://dolarhoje.com", "image": "dolar.svg"},
    {
        "name": "Dólar Turismo",
        "url": "https://dolarhoje.com/dolar-turismo/",
        "image": "dolar.svg",
    },
    {
        "name": "Euro",
        "url": "https://dolarhoje.com/euro-hoje/",
        "image": "euro.svg",
    },
    {
        "name": "Libra",
        "url": "https://dolarhoje.com/libra-hoje/",
        "image": "libra.svg",
    },
    {
        "name": "Peso Argentino",
        "url": "https://dolarhoje.com/peso-argentino/",
        "image": "peso.svg",
    },
    {
        "order": 5,
        "name": "Ouro",
        "url": "https://dolarhoje.com/ouro-hoje/",
        "image": "ouro.svg",
    },
    {
        "name": "Bitcoin",
        "url": "https://dolarhoje.com/bitcoin-hoje/",
        "image": "bitcoin.svg",
    },
    {
        "name": "Ethereum",
        "url": "https://dolarhoje.com/ethereum/",
        "image": "ethereum.svg",
    },
    {
        "name": "Dogecoin",
        "url": "https://dolarhoje.com/dogecoin-hoje/",
        "image": "dogecoin.svg",
    },
    {
        "name": "Dash",
        "url": "https://dolarhoje.com/dash/",
        "image": "dash.svg",
    },
    {
        "name": "Ripple",
        "url": "https://dolarhoje.com/ripple-hoje/",
        "image": "ripple.svg",
    },
    {
        "name": "Litecoin",
        "url": "https://dolarhoje.com/litecoin/",
        "image": "litecoin.svg",
    },
]

dolar_bc = [{"name": "purchase_tax"}, {"name": "sale_tax"}]

print(f"The DELAY was set to {DELAY} minutes")


@repeat(every(DELAY).minutes)
def scrapy_dolar_hoje():
    datetime = dt.now()
    print(f"Dólar Hoje - Scrapy started at: {datetime}")

    for item, value in enumerate(coins_data):
        url = value["url"]
        html_doc = scraper.get(url).content
        soup = BeautifulSoup(html_doc, "html.parser")
        coin_value = float(
            soup.find(id="nacional").get("value").replace(",", ".")
        )

        try:
            coins_collection.insert_one(
                {
                    "order": item,
                    "name": value["name"],
                    "value": coin_value,
                    "image": value["image"],
                    "datetime": datetime,
                }
            )
        except Exception as e:
            print(f"Error to insert data into colletion: {e}")

    print(f"Dólar Hoje - Scrapy finished at: {datetime}")


@repeat(every().day.at(CHECK_DOLAR_BC))
def scrapy_dolar_bc():
    datetime = dt.now()
    print(f"Dólar BC - Scrapy started at: {datetime}")

    for item, value in enumerate(dolar_bc):
        json = requests.get(CHECK_DOLAR_BC_URL).json()

        name = (
            "Dólar BC - Compra"
            if value["name"] == "purchase_tax"
            else "Dólar BC - Venda"
        )
        value = json[value["name"]]
        date = dt.strptime(json["date"], "%d/%m/%Y")

        try:
            coins_collection.insert_one(
                {
                    "order": item,
                    "name": name,
                    "value": value,
                    "image": "dolar.svg",
                    "datetime": date,
                }
            )
        except Exception as e:
            print(f"Error to insert data into colletion: {e}")

    print(f"Dólar BC - Scrapy finished at: {datetime}")


while True:
    run_pending()
    time.sleep(1)
