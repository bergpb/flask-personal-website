beautifulsoup4==4.9.3
bs4==0.0.1
certifi==2020.12.5
cfscrape==2.1.1
chardet==4.0.0
click==7.1.2
dnspython==1.16.0
Flask==2.0.2
Flask-PyMongo==2.3.0
gitdb==4.0.7
GitPython==3.1.14
idna==2.10
itsdangerous==2.0.1
Jinja2==3.0.3
MarkupSafe==2.0.1
pymongo==3.11.3
python-dotenv==0.17.0
requests==2.25.1
smmap==4.0.0
soupsieve==2.2.1
urllib3==1.26.4
Werkzeug==2.0.2
schedule==1.1.0
Flask_DebugToolbar==0.13.1
