.PHONY: env up down logs restart ps build build-and-push

COMPOSE_FILE := docker-compose.yml
NAME         := bergpb/flask-personal-website
TAG          := ${VERSION}
IMG          := ${NAME}:${TAG}
LATEST       := ${NAME}:latest

ifeq ($(ENV),development)
	COMPOSE_FILE := docker-compose-dev.yml
endif

env:
	@direnv allow

up:
	@docker compose -f $(COMPOSE_FILE) up -d

down:
	@docker compose -f $(COMPOSE_FILE) down

logs:
	@docker compose -f $(COMPOSE_FILE) logs -f

restart:
	@docker compose -f $(COMPOSE_FILE) restart

create-db: up
	@docker compose -f $(COMPOSE_FILE) exec web-dev sh -c "python create_db.py"
	@$(MAKE) down

ps:
	@docker compose -f $(COMPOSE_FILE) ps

build:
	@docker build --build-arg VERSION="${VERSION}" . -t ${NAME}:${TAG}
	@docker tag ${IMG} ${LATEST}

build-no-cache:
	@docker build --build-arg VERSION="${VERSION}" . -t ${NAME}:${TAG} --no-cache
	@docker tag ${IMG} ${LATEST}

build-and-push: build
	@docker push ${IMG}
	@docker push ${LATEST}
