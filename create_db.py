import os
import sqlite3
from datetime import date, timedelta, datetime as dt

aux = 0
current_day = dt.now()
number_of_washers = 3

order = [
    {"id": 0, "name": "Cilene", "gender": 1},
    {"id": 1, "name": "Raimundo", "gender": 0},
    {"id": 2, "name": "Brenda", "gender": 1},
    {"id": 3, "name": "Lindembergue", "gender": 0},
]

start_date = date(2022, 5, 24)
end_date = date(current_day.year, 12, 31)

delta = end_date - start_date

if not os.path.isfile("dql.db"):
    with sqlite3.connect("dql.db") as con:
        cur = con.cursor()

        cur = con.cursor()

        cur.execute(
            """CREATE TABLE washer_of_the_day (id INTEGER PRIMARY KEY AUTOINCREMENT, washer_id integer, date date)"""
        )

        cur.execute(
            """CREATE TABLE washers (id INTEGER PRIMARY KEY AUTOINCREMENT, name text, gender_id integer, sequence integer)"""
        )

        cur.execute(
            """INSERT INTO washers (name, gender_id, sequence) VALUES ('Cilene', 1, 0), ('Raimundo', 0, 1), ('Brenda', 1, 2), ('Lindembergue', 0, 3)"""
        )

        for i in range(delta.days + 1):
            day = start_date + timedelta(days=i)
            washer_id = order[aux]["id"]

            cur.execute(
                """
                INSERT INTO washer_of_the_day (washer_id, date) VALUES (?, ?)
            """,
                (washer_id, day),
            )

            if aux < number_of_washers:
                aux += 1
            else:
                aux = 0

    print("Tables and data inserted!")

    con.commit()
else:
    print("Database already exists!!!")
